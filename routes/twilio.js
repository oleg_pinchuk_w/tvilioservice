var express = require('express');
var router = express.Router();
var psql = require('../models/psqlService');
var tvilio = require('../models/tvilioService');

/* GET users listing. */
router.post('/send', function(req, res, next) {
    const data = {message: req.body.message, to: req.body.phone};
    var result = {};
    psql.getCredentails.then(function(resp){
        var client = tvilio.auth(resp.data.sid,resp.data.token);
        tvilio.send(client,data.message,data.to, '+15005550006').then(function(resp){ //how we get  'from' phone number
           result = {
               status:200 , message: 'OK', body: resp
           };
           res.status(result.status);
           res.send(result);
        }).catch(function(reject){
            result = {
                status: reject.status,
                message: reject.message,
                body: reject
            };
            res.status(result.status);
            res.send(result);
        }); //change to real from phone
    }).catch(function(reject){
        result = {
        status: reject.status,
        message: reject.message,
        body: reject
    };
        res.status(result.status);
        res.send(result);
    });

});

module.exports = router;
