var twilio = require('twilio');
module.exports = {
    auth: function (cid,token) {
        // Get a Postgres client from the connection pool
        return client = new twilio.RestClient(cid, token);
    },
    send: function (client, message, to , from) {
           return new Promise(function (resolve,reject) {
            client.messages.create({
                body: message,
                to: to,  // Text this number
                from: from // From a valid Twilio number
            }, function (err, message) {
                if (err) {
                    reject(err);
                } else {
                    resolve(message);
                }
            });
        })
    }

};