var express = require('express');
const pg = require('pg');
const path = require('path');
var config = {
    user: 'postgres', //env var: PGUSER
    database: 'singlepay', //env var: PGDATABASE
    password: '1', //env var: PGPASSWORD
    host: 'localhost', // Server hosting the postgres database
    port: 5433 //env var: PGPORT
};

module.exports = {
    getCredentails: new Promise(function (resolve,reject) {
        pg.connect(config, function (err, client, done) {
            var results = {};
            // Handle connection errors
            if(err) {
                done();
                return console.error('error on connect', reject(err));
            }

            const query = client.query('SELECT * FROM tvilio_config');
            query.on('row', function (row){
                results.data = row;
            });
            // After all data is returned, close connection and return results
            query.on('end', function(){
                done();
                resolve(results);
            });
        });
    })
};